<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PlanEvento
 *
 * @ORM\Table(name="planesEventos", indexes={@ORM\Index(name="idEvento", columns={"idEvento"})})
 * @ORM\Entity
 */
class PlanEvento
{
    /**
     * @var string
     *
     * @ORM\Column(name="detalle", type="text", length=65535, nullable=false)
     */
    private $detalle;

    /**
     * @var float
     *
     * @ORM\Column(name="importe", type="float", precision=10, scale=0, nullable=false)
     */
    private $importe;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activo", type="boolean", nullable=false)
     */
    private $activo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaHora", type="datetime", nullable=false)
     */
    private $fechahora = 'CURRENT_TIMESTAMP';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Evento
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Evento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idEvento", referencedColumnName="id")
     * })
     */
    private $idevento;


}

