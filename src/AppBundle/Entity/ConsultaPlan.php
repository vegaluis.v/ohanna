<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ConsultaPlan
 *
 * @ORM\Table(name="consultasPlanes", indexes={@ORM\Index(name="idPlan", columns={"idPlan"})})
 * @ORM\Entity
 */
class ConsultaPlan
{
    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=false)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="consulta", type="text", length=65535, nullable=false)
     */
    private $consulta;

    /**
     * @var boolean
     *
     * @ORM\Column(name="visible", type="boolean", nullable=false)
     */
    private $visible;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaHora", type="datetime", nullable=false)
     */
    private $fechahora = 'CURRENT_TIMESTAMP';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\PlanEvento
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\PlanEvento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idPlan", referencedColumnName="id")
     * })
     */
    private $idplan;


}

