<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RespuestaConsulta
 *
 * @ORM\Table(name="respuestasConsultas", indexes={@ORM\Index(name="idConsulta", columns={"idConsulta"})})
 * @ORM\Entity
 */
class RespuestaConsulta
{
    /**
     * @var string
     *
     * @ORM\Column(name="respuesta", type="text", length=65535, nullable=false)
     */
    private $respuesta;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaHora", type="datetime", nullable=false)
     */
    private $fechahora = 'CURRENT_TIMESTAMP';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\ConsultaPlan
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ConsultaPlan")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idConsulta", referencedColumnName="id")
     * })
     */
    private $idconsulta;


}

